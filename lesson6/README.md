# 文件说明
> 微信小程序API应用（上）

## Location
- 查看附近美食餐厅案例
- 需要使用[*腾讯地图SDK*](https://lbs.qq.com/)

## UserCenter
- 用户个人中心案例
- 需要使用[*聚合数据API*](https://www.juhe.cn/)

## UserSignIn
- 用户登录案例

## Weather
- 天气预报案例
- 需要使用[*百度地图开放平台API*](http://lbs.baidu.com/)

## XMind
- 微信小程序API(上)章节的思维导图文件
