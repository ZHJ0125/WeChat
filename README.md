![](https://img.shields.io/badge/Creator-ZhangH.J.-success)
![Total visitor](https://visitor-count-badge.herokuapp.com/total.svg?repo_id=${https://github.com/ZHJ0125/WeChat})
![](https://img.shields.io/github/last-commit/ZHJ0125/WeChat)


# 文件说明
> 微信小程序练习代码  
> 点击链接查看 [项目简介页面](https://zhj0125.github.io/WeChat/)

## 00_Report
- 用于存放实验报告

## lesson1
- 使用多种方式实现数值比较功能

## lesson2
- 编写调查问卷,实现服务器数据提交

## lesson3
- 一个简单的计算器

## lesson4
- 歌曲推荐及音乐播放器项目

## lesson5
- 婚礼邀请函(未完成)

## lesson6
- 微信API应用
- 包括：**用户登录**、**用户中心**、**天气预报**、**地理位置信息**四个项目，以及微信小程序API章节的**思维导图文件**

