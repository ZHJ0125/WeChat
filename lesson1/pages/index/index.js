// pages/index/index.js
Page({

  /**
   * 页面的初始数据
   */
  data: {
    num1: '',
    num2: '',
    result: ''
  },

  /**
   * input双事件处理函数方式1
   */
  // num1: 0,
  // num2: 0,
  // num1change: function(e) {
  //   this.num1 = Number(e.detail.value)
  //   console.log('第1个数字为' + this.num1)
  // },
  // num2change: function (e) {
  //   this.num2 = Number(e.detail.value)
  //   console.log('第2个数字为' + this.num2)
  // },
  // end of numchange function

  /**
   * input单事件处理函数方式2,通过 id 区分不同的input组件
   */
  // inputchange: function (e) {
  //   this[e.currentTarget.id] = Number(e.detail.value)
  //   console.log("第" + e.currentTarget.id + "个数字的值是:" + this[e.currentTarget.id])
  //   // console.log(this.num1)
  //   // console.log(this.num2)
  // },

  /**
   * input单事件处理函数方式2,通过 dataset 区分不同的input组件
   */
  // inputchange: function (e) {
  //   this[e.target.dataset.id] = Number(e.detail.value)
  //   console.log("第" + e.target.dataset.id + "个数字的值是:" + this[e.target.dataset.id])
  //   // console.log(this.num1)
  //   // console.log(this.num2)
  // },


  /**
   * button事件处理函数
   */
  // compare: function (e) {
  //   console.log('比较按钮被点击了')
  //   // console.log(e)
  //   var str = '两数相等'
  //   if(this.num1 > this.num2) {
  //     str = '第 1 个数大'
  //   }
  //   else if (this.num1 < this.num2) {
  //     str = '第 2 个数大'
  //   }
  //   this.setData({result: str})
  //   console.log(str)
  // },
  // end of compare function

  
  /**
   * input事件处理函数方式3,条件运算符直接在WXML中进行比较
   */
  // change3: function(e) {
  //   var data = {}
  //   data[e.target.dataset.id] = Number(e.detail.value)
  //   this.setData(data)
  //   console.log(data)
  // },


  /**
   * input事件处理函数方式4,条件渲染显示不同的标签结果
   */
  // change4: function(e) {
  //   var data = {}
  //   data[e.target.dataset.id] = Number(e.detail.value)
  //   this.setData(data)
  //   console.log(data)
  // },


  /**
   * input事件处理函数方式5,通过表单形式获取input组件的值
   */
  fromCompare: function(e) {
    var str = "两数相等"
    var num1 = Number(e.detail.value.num1)
    var num2 = Number(e.detail.value.num2)
    if(num1 > num2) {
      str = "第 1 个数大"
    }
    else if(num1 < num2) {
      str = "第 2 个数大"
    }
    this.setData({result: str})
  },


  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
    // console.log('页面加载')
    // console.log(options)
  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {
    // console.log('页面初次渲染完成')
    // wx.navigateTo({
    //   url: '/pages/test/test?name=value1&name2=value2',
    // })
    // console.log('页面转换完成')
  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {
    // console.log('页面显示')
  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {
    console.log('此时用户下拉刷新')
  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {
    console.log('此时用户下拉触底')
  },

  /**
   * 页面滚动事件的处理函数
   */
  onPageScroll: function () {
    console.log('此时用户正在滚动页面')
  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function () {

  }
})