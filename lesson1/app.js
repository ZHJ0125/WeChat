App ({
  // 生命周期回调——检测小程序初始化
  // 小程序初始化完成时触发(全局只触发一次)
  onLaunch: function(options) {
    // console.log('onLaunch执行')
    // console.log(options)
  },

  // 生命周期回调——监听小程序显示
  // 小程序启动或进入前台时触发
  onShow: function (options) {
    // console.log('onShow执行')
    // console.log(options)
  },

  // 生命周期回调——监听小程序隐藏
  // 小程序进入后台时触发
  onHide: function () {
    console.log('onHide执行')
  },

  // 错误监听函数
  // 小程序发生脚本变化,或调用API失败时触发
  onError: function (error) {
    console.log('onError执行')
    console.log(error)
  },

  // 页面不存在监听函数
  // 小程序要打开的页面不存在时触发
  onPageNotFound: function (options) {
    console.log('onPageNotFound执行')
    console.log(options)
  }
})